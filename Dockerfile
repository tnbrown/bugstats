FROM python:3.6-alpine

RUN apk update \
 && apk add --no-cache build-base libffi-dev libressl-dev

WORKDIR /bugstats
# first copy just requirements.txt and run pip
COPY bugstats/requirements.txt /bugstats/bugstats/
RUN pip --no-cache-dir install -r /bugstats/bugstats/requirements.txt
# then copy in latest repo., to maximize caching
COPY bugstats-art/ /bugstats/bugstats-art/ 
COPY bugstats/ /bugstats/bugstats/
