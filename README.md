# bugstats

Calculate metrics for contributions to launchpad -> GitLab bug migration.

4906 [launchpad bugs](https://bugs.launchpad.net/inkscape),
223 (open) [GitLab bugs](https://gitlab.com/groups/inkscape/-/issues)