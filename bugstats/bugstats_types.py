from collections import namedtuple

BugClosure = namedtuple("BugClosure", "gl_bug_id lp_bug_id gl_user time")
Discovery = namedtuple("Discovery", "gl_user time")
# GitLab issues have a global ID, but you can't use it to access the issue
# through the API, you need the project name/id and `iid`, the within project
# non-global issue number
GL_Bug_Id = namedtuple("GL_Bug_Id", "project_id iid")
Level = namedtuple("Level", "name score")
Special = namedtuple("Special", "pts comment bio color bugname")
ScoreNote = namedtuple("ScoreNote", "comment pts lp_bug_id")
SpecialScoreNote = namedtuple(
    "SpecialScoreNote", "special comment pts lp_bug_id bugname"
)
