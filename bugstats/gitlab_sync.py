"""
Convert Launchpad to Gitlab bugs

ID vs. IID

Gitlab bugs have a globally unique ID, which you can't use to fetch
them, instead you have to fetch them by IID + PROJECT_ID.  IID is
only unique within projects.  Incoming bugs may be spread among projects
in the inkscape group, so use ID to manage bugs and maintain GL_BUG_MAP
to map ID to IID + PROJECT_ID.
"""

import os
import time
import random  # for faking results for testing
import re
from datetime import datetime
from dateutil.parser import parse

import gitlab

import keyvalstore
from keyvalstore import get_val, put_val, drop_val

from bugstats_types import BugClosure, GL_Bug_Id

lp_src_re = re.compile(
    r"Migrated[- ]*from:?\s*https:*/*bugs.launchpad.net/inkscape/\+bug/(\d+)",
    flags=re.IGNORECASE,
)

DB_FILENAME = "bugstat.db.sqlite"

PROJ_ID = 3472737
PROJ_ID = "inkscape/inkscape"
GROUP_ID = "inkscape"

GL_BUG_MAP = {}  # map GL bug ID to project ID + IID


def _get_connection(connect_info):
    """Create a connection, if needed, and cache in `connect_info`

    Args:
        connect_info (list): cache for connection

    Returns:
        [connection, projects_cache_dict]

    FIXME: projects cache not needed anymore?
    """
    if not connect_info:
        test_token = open(os.environ['TEST_TOKEN']).read().strip()
        # needed to iterate notes (not issues)
        gl = gitlab.Gitlab('https://gitlab.com/', private_token=test_token)
        connect_info[:] = [gl, {}]  # [connection, project_cache]
    return connect_info


def fetch_gl_projects(__connection=[]):
    """fetch the list of GL projects"""
    gl, projects = _get_connection(__connection)
    group = gl.groups.get(GROUP_ID)
    return {i.id: i.attributes for i in group.projects.list()}


def fetch_gl_bug(gl_bug_id, __connection=[]):
    """Fetch bug data from Launchpad

    __connection *is not an argument*, it's a cached DB connection,
    usage is: `fetch_gl_bug(gl_bug_id)`
    """
    gl, projects = _get_connection(__connection)

    bug = gl.projects.get(gl_bug_id.project_id, lazy=True).issues.get(
        gl_bug_id.iid
    )
    ans = dict(bug.attributes)
    ans.update(
        {
            'author': {
                'name': bug.author['name'],
                'username': bug.author['username'],
            },
            'notes': [],
        }
    )
    print(gl_bug_id, ans['author']['username'])
    # project = gl.projects.get(bug.project_id, lazy=True)
    # editable_issue = project.issues.get(bug.iid, lazy=True)
    page = 0
    # notes = editable_issue.notes.list(page=page)
    notes = bug.notes.list(page=page)
    # notes are returned in reverse chronological order
    while notes:
        ans['notes'].extend(
            {
                'author': {
                    'name': note.author['name'],
                    'username': note.author['username'],
                },
                'text': note.body,
            }
            for note in notes
        )
        page += 1
        # notes = editable_issue.notes.list(page=page)
        notes = bug.notes.list(page=page)

    return ans


def get_gl_bug(gl_bug_id, max_age=None):
    """Get a dict representing a Launchpad bug.

    If our (DB) cache entry is older than max_age seconds, check
    remote for updates.

    Args:
        gl_bug_id (int): Launchpad bug ID
        max_age (float): max. age of our cache entry, default: no age limit

    Returns: dict representing Launchpad bug.
    """
    gl_bug_id = GL_Bug_Id(*gl_bug_id)  # JSON turns it into a list
    key = ('gl_bug', gl_bug_id)
    bug = get_val(key)

    now = time.time()
    if bug == {} or (
        max_age is not None and now - bug['last_fetch'] > max_age
    ):
        print("Fetching GitLab issue #%s" % (gl_bug_id,))
        bug['bug'] = fetch_gl_bug(gl_bug_id)
        bug['last_fetch'] = now
        put_val(key, bug)
    return bug


def update_gl_issues(max_age=3600, last_n_days=3):
    """update GitLab issues updated since the last time we updated

    Updates `gl_last_update`.

    Args:
        max_age (int): do nothing if we last updated less than max_age
            seconds ago
        last_n_days (int): if not previous update, go back this many days,
                           also the max. number of days to go back

    Or updated in the `last_n_days`, if we have no prev. update.
    """

    prev = get_val('gl_last_update') or 0
    update_start = time.time()
    gl_bug_ids = set(GL_Bug_Id(*i) for i in get_val('gl_bug_ids'))
    if update_start - prev < max_age:
        print("Skipping GitLab update")
        print("%d issues known" % len(gl_bug_ids))
        return
    max_back = time.time() - 86400 * last_n_days
    if not prev or prev < max_back:
        prev = max_back
    test_token = open(os.environ['TEST_TOKEN']).read().strip()
    gl = gitlab.Gitlab('https://gitlab.com/', private_token=test_token)
    group = gl.groups.get(GROUP_ID)
    since = datetime.fromtimestamp(prev)
    print("Querying GitLab for updated issues")
    page = 0
    issues = group.issues.list(updated_after=since, page=page)
    while issues:
        for issue in issues:
            # update local cache copy of issue
            # X GL_BUG_MAP[issue.id] = (issue.project_id, issue.iid)
            gl_bug_id = GL_Bug_Id(issue.project_id, issue.iid)
            get_gl_bug(gl_bug_id, max_age=0)
            gl_bug_ids.add(gl_bug_id)
        page += 1
        issues = group.issues.list(updated_after=since, page=page)
    # VERY IMPORTANT that `gl_last_update` is the time we STARTED
    # updating and not the time we finished updating, as that would
    # skip updates made during the update process (next time).
    put_val('gl_last_update', update_start)
    put_val('gl_bug_ids', list(gl_bug_ids))


def get_gl_userlist():
    """Get list of usernames for GL users
    Returns: list
    """
    users = []
    for gl_bug_id in get_val('gl_bug_ids'):
        bug = get_gl_bug(gl_bug_id, max_age=1000000000)
        users.append(bug['bug']['author']['username'])
    return users


def scoring_gl_issues():
    """List of GitLab issue ids for which claim to close a LaunchPad issue

    Returns a set of (gl_bug_id, lp_bug_id, gl_username) (int, int, str)
    tuples, i.e. gl_username claims credit for lp_bug_id in gl_bug_id.
    """

    scores = set()
    if os.environ.get('BUGSTATS_FAKE_GL'):
        lp_bug_ids = sorted(get_val('all_lp_bug_ids'))
        # sort so we keep using the same bugs to minimizing fetching
    for gl_bug_id in set(GL_Bug_Id(*i) for i in get_val('gl_bug_ids')):
        bug = get_gl_bug(gl_bug_id)
        if not bug['bug']['notes']:
            continue
        txt = bug['bug']['description'] + ' ' + bug['bug']['notes'][-1]['text']
        matches = lp_src_re.finditer(txt)
        hit = False
        for match in matches:
            hit = True
            scores.add(
                BugClosure(
                    gl_bug_id=gl_bug_id,
                    lp_bug_id=int(match.group(1)),
                    gl_user=bug['bug']['author']['username'],
                    time=parse(bug['bug']['updated_at']).timestamp(),
                )
            )
        if (
            not hit
            and os.environ.get('BUGSTATS_FAKE_GL')
            and lp_bug_ids
            and random.uniform(0, 1) < 0.2
        ):
            text = bug['bug']['notes'][-1]['text']
            text += (
                "\nMigrated from: https://bugs.launchpad.net/inkscape/+bug/%s"
                % lp_bug_ids.pop(0)
            )
            matches = lp_src_re.finditer(text)
            hit = False
            for match in matches:
                hit = True
                scores.add(
                    BugClosure(
                        gl_bug_id=gl_bug_id,
                        lp_bug_id=int(match.group(1)),
                        gl_user=bug['bug']['author']['username'],
                        time=parse(bug['bug']['updated_at']).timestamp(),
                    )
                )
            assert hit, text

    return scores


def main():
    keyvalstore.DB_FILENAME = DB_FILENAME
    GL_BUG_MAP.update(get_val('gl_bug_map'))
    try:
        pass
        # pprint(get_gl_bug(15650185, max_age=None))
        # put_val('gl_last_update', time.time()-86400 * 100)
        drop_val('gl_last_update')
        update_gl_issues(last_n_days=10)
        # print(get_val('gl_bug_ids'))
        # print(fake_scoring_gl_issues())
        # gl_bug_ids = get_val('gl_bug_ids')
        # for i in gl_bug_ids:
        #     print(get_val(('gl_bug', i))['bug']['author']['username'])
        # projects = fetch_gl_projects()
        # one = projects[next(iter(projects.keys()))]
        # print(one.attributes)
        # print(dir(one))
        # put_val('gl_project', projects)
    finally:
        put_val('gl_bug_map', GL_BUG_MAP)


if __name__ == "__main__":
    main()
