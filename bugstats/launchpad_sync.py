"""
launchpad_sync.py - collect bug data from Launchpad

TerryNBrown@gmail.com Sat Nov 10 11:58:27 CST 2018
"""
import time
from datetime import datetime

# from pprint import pprint
import os
import random  # for faking results for testing
import re
import sys

from launchpadlib.launchpad import Launchpad

import keyvalstore
from keyvalstore import get_val, put_val

from gitlab_sync import get_gl_userlist
from bugstats_types import BugClosure

closer_re = re.compile(
    r"Closed[- ]*by:?\s*https:*/*gitlab.com/(?!\d)(\w{2,20})",
    flags=re.IGNORECASE,
)
# NOTE: not used
moved_re = re.compile(
    r"Moved[- ]*to:?\s*https:*/*gitlab.com/inkscape/(\w+)/issues/(\d+)",
    flags=re.IGNORECASE,
)

CACHEDIR = "/home/tbrown/t/Proj/bugstats/lp_cache"


def dt_to_ts(dt):
    return time.mktime(dt.timetuple())


def fetch_lp_bug(lp_bug_id, return_bug=False, __connection=[]):
    """Fetch bug data from Launchpad

    __connection *is not an argument*, it's a cached DB connection,
    usage is: `fetch_lp_bug(lp_bug_id)`
    """
    if not __connection:
        if os.environ.get("BUGSTATS_MODIFY"):
            __connection[:] = [
                Launchpad.login_with(
                    'Inkscape Bug Migration',
                    'production',
                    CACHEDIR,
                    version='devel',
                )
            ]
        else:
            __connection[:] = [
                Launchpad.login_anonymously(
                    'just testing', 'production', CACHEDIR, version='devel'
                )
            ]
    launchpad = __connection[0]

    bug = launchpad.bugs[lp_bug_id]
    ans = {k: getattr(bug, k) for k in bug.lp_attributes}
    for k, v in list(ans.items()):
        if isinstance(v, datetime):
            ans[k] = dt_to_ts(v)
    ans['attachment_count'] = len(bug.attachments)
    ans['messages'] = []
    for message in bug.messages:
        try:
            # can fail if user is suspended - accessing the attribute
            # generates an exception.
            owner_name = message.owner.name
            owner_display = message.owner.display_name
        except Exception:
            owner_name = 'UNAVAILABLE'
            owner_display = 'UNAVAILABLE'
        ans['messages'].append(
            {
                'content': message.content,
                'date_created': dt_to_ts(message.date_created),
                'author': {'username': owner_name, 'name': owner_display},
            }
        )

    if return_bug:
        return ans, bug
    else:
        return ans


def get_lp_bug(lp_bug_id, max_age=None, status=None, fetch=True):
    """Get a dict representing a Launchpad bug.

    If our (DB) cache entry is older than max_age seconds, check
    remote for updates.

    Args:
        lp_bug_id (int): Launchpad bug ID
        max_age (float): max. age of our cache entry, default: no age limit
        status (str): bug's status, passed in from task
        fetch (bool): set to False to prevent fetching bug from LP

    Returns: dict representing Launchpad bug.
    """
    key = ('lp_bug', lp_bug_id)
    bug = get_val(key)

    now = time.time()
    if fetch and (
        bug == {}
        or (max_age is not None and now - bug['last_fetch'] > max_age)
    ):
        print("Fetching Launchpad issue #%s" % lp_bug_id)
        bug['bug'] = fetch_lp_bug(lp_bug_id)
        bug['last_fetch'] = now
        if status is not None:
            bug['status'] = status
        put_val(key, bug)
    return bug


def update_lp_issues(max_age=3600, last_n_days=10):
    """update Launchpad issues updated since the last time we updated

    Updates `lp_last_update`.

    Args:
        max_age (int): do nothing if we last updated less than max_age
            seconds ago
        last_n_days (int): if not previous update, go back this many days

    Or updated in the `last_n_days`, if we have no prev. update.
    """

    prev = get_val('lp_last_update') or 0
    update_start = time.time()
    if update_start - prev < max_age:
        print("Skipping Launchpad update")
        return
    max_back = time.time() - 86400 * last_n_days
    if not prev or prev < max_back:
        prev = max_back
    if os.environ.get('BUGSTATS_SETBACK'):
        # dev. - include previous x hours for testing
        prev -= 3600 * int(os.environ['BUGSTATS_SETBACK'])
    lp = Launchpad.login_anonymously(
        'just testing', 'production', CACHEDIR, version='devel'
    )
    since = datetime.fromtimestamp(prev)
    print("Querying Launchpad for updated issues")
    lp_bug_ids = set(get_val('lp_bug_ids'))
    inkscape = lp.projects['inkscape']
    # thanks to https://askubuntu.com/a/809967/196323
    tasks = inkscape.searchTasks(
        modified_since=since,
        status=[
            'New',
            'Incomplete',
            'Triaged',
            'Opinion',
            'Invalid',
            'Won\'t Fix',
            'Confirmed',
            'In Progress',
            'Fix Committed',
            'Fix Released',
        ],
    )
    for task in tasks:
        # update local cache copy of issue
        get_lp_bug(task.bug.id, max_age=0, status=task.status)
        lp_bug_ids.add(task.bug.id)
    # VERY IMPORTANT that `gl_last_update` is the time we STARTED
    # updating and not the time we finished updating, as that would
    # skip updates made during the update process (next time).
    put_val('lp_last_update', update_start)
    put_val('lp_bug_ids', list(lp_bug_ids))

    if 0:
        launchpad = Launchpad.login_anonymously(
            'just testing', 'production', CACHEDIR, version='devel'
        )
        inkscape = launchpad.projects['inkscape']
        print(dir(inkscape))
        print(inkscape.lp_attributes)
        print(inkscape.lp_collections)
        print(inkscape.lp_entries)
        tasks = inkscape.searchTasks()
        for task in tasks[:10]:
            print(task.title)
        bug = tasks[0].bug
        print(bug.description)  # == bug.messages[0].content
        print(bug.messages[1].content)


def get_all_lp_bugs():
    """Get list of bugs to assign specials to"""
    lp = Launchpad.login_anonymously(
        'just testing', 'production', CACHEDIR, version='devel'
    )
    print("Querying Launchpad for list of issues")
    lp_bug_ids = set()
    inkscape = lp.projects['inkscape']
    tasks = inkscape.searchTasks()
    for task in tasks:
        # don't use task.bug as that would fetch more data from LP
        lp_bug_id = task.web_link.rsplit('/', 1)[-1]
        print(lp_bug_id)
        lp_bug_ids.add(lp_bug_id)
    return lp_bug_ids


def get_all_lp_status():
    """Get status for all bugs and update our records, a legacy
    correction from when status wasn't collected.
    """
    lp = Launchpad.login_anonymously(
        'just testing', 'production', CACHEDIR, version='devel'
    )
    print("Querying Launchpad for status")
    inkscape = lp.projects['inkscape']
    # search for closed as well
    tasks = inkscape.searchTasks(
        status=[
            'New',
            'Incomplete',
            'Triaged',
            'Opinion',
            'Invalid',
            'Won\'t Fix',
            'Confirmed',
            'In Progress',
            'Fix Committed',
            'Fix Released',
        ]
    )
    for task in tasks:
        # don't use task.bug as that would fetch more data from LP
        lp_bug_id = int(task.web_link.rsplit('/', 1)[-1])
        # print(lp_bug_id)
        bug = get_lp_bug(lp_bug_id, fetch=False)
        if bug:
            key = ('lp_bug', lp_bug_id)
            bug['status'] = task.status
            put_val(key, bug)
            print(key, bug['status'])


def get_closer(text):
    closer = closer_re.search(text)
    return (
        closer.group(1) if closer and closer.group(1) != 'NOT_CLOSED' else None
    )


def get_moved_to(text):
    """NOTE: not used"""
    moved = moved_re.search(text)
    return (moved.group(1), moved.group(2)) if moved else None


def scoring_lp_issues():
    ans = []
    if os.environ.get("BUGSTATS_FAKE_LP"):
        # print(get_gl_bug(get_val('gl_bug_ids')[0], max_age=1000000000))
        # print(get_val('gl_bug_ids'))
        users = get_gl_userlist()[:20]
    for lp_bug_id in get_val('lp_bug_ids'):
        bug = get_lp_bug(lp_bug_id)
        msgs = bug['bug'].get('messages', [])
        if msgs:
            txt = '\n'.join(reversed(list(i['content'] for i in msgs)))
            closer = get_closer(txt)
            if (
                not closer
                and os.environ.get("BUGSTATS_FAKE_LP")
                and random.uniform(0, 1) > 0.2
            ):
                text = msgs[-1]['content'] + '\n'
                text += (
                    "Closed by: https://gitlab.com/%s"
                    % random.sample(users, 1)[0]
                )
                closer = get_closer(text)
                assert closer
            if closer:
                ans.append(
                    BugClosure(
                        lp_bug_id=lp_bug_id,
                        gl_user=closer,
                        time=bug['bug']['date_last_updated'],
                        gl_bug_id=None,
                    )
                )
        else:
            # just a legacy data issue I think
            print("ERROR - no messages for %s" % lp_bug_id)

    return ans


def lp_add_migration_tag(lp_bug_id, tag='bug-migration'):
    """set bug-migration flag on bug"""
    # get the current version of the bug
    data, bug = fetch_lp_bug(lp_bug_id, return_bug=True)
    if tag in data['tags']:
        print(f"Tag '{tag}' already present in bug {lp_bug_id}")
        return
    bug.tags = bug.tags + [tag]
    bug.lp_save()


def main():
    keyvalstore.DB_FILENAME = os.envrion(['DB'])
    # pprint(get_lp_bug(1772883, max_age=None))
    # put_val('lp_last_update', time.time())
    # lp_bug_ids = set(get_val('lp_bug_ids'))
    # for key in keys_starting('lp_bug'):
    #     key = eval(key)
    #     lp_bug_ids.add(key[1])
    #     print(repr(key))
    # put_val('lp_bug_ids', list(i for i in lp_bug_ids if isinstance(i, int)))
    if len(sys.argv) > 1:
        if sys.argv[1] == 'DELETE_LP_BUGS':
            from keyvalstore import keys_starting, drop_val

            count = 0
            for key in keys_starting('lp_bug'):
                count += 1
                drop_val(key)
            print("%s dropped" % count)
            return
        get_lp_bug(int(sys.argv[1]), max_age=0)
        return
    update_lp_issues(last_n_days=10)


if __name__ == "__main__":
    main()
