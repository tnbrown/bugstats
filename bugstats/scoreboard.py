import argparse
import csv
import json
import os
import random
import time
from collections import defaultdict
from random import shuffle

import jinja2

from keyvalstore import get_val, put_val
import keyvalstore

from gitlab_sync import (
    get_gl_bug,
    get_gl_userlist,
    scoring_gl_issues,
    update_gl_issues,
)
from launchpad_sync import (
    get_lp_bug,
    scoring_lp_issues,
    get_all_lp_bugs,
    update_lp_issues,
    get_all_lp_status,
    lp_add_migration_tag,
)

from bugstats_types import (
    BugClosure,
    Discovery,
    Level,
    Special,
    ScoreNote,
    SpecialScoreNote,
)

# these are filled in by load_data()
LEVELS = []
SPECIALS = {}
SPECIAL = {}

UNCLOSED = ['Triaged', 'Confirmed', 'In Progress']

# number of bugs to close to complete game, somewhat arbitrary
TOTAL_BUGS = 4558


def make_parser():

    parser = argparse.ArgumentParser(
        description="""Generate scoreboard for bug migration""",
        formatter_class=argparse.ArgumentDefaultsHelpFormatter,
    )

    required = parser.add_argument_group("Required arguments")
    required.add_argument(
        "--art", help="Path to art dir.", metavar="PATH", required=True
    )
    required.add_argument(
        "--db", help="Path to database", metavar="PATH", required=True
    )
    required.add_argument(
        "--build",
        help="Path to build dir.",
        metavar="BUILD_PATH",
        required=True,
    )
    mode = parser.add_argument_group("Mode of operation (pick one)")
    mode.add_argument(
        "--update-gitlab",
        help="Refresh GitLab info for changes in the last SECONDS",
        metavar='SECONDS',
        type=int,
    )
    mode.add_argument(
        "--update-launchpad",
        help="Refresh Launchpad info for changes in the last SECONDS",
        metavar='SECONDS',
        type=int,
    )
    mode.add_argument(
        "--generate-specials",
        help="Create BUILD_PATH/specials.json (one time bootstrap op.)",
        metavar='FILE',
    )
    mode.add_argument(
        "--fix-status",
        help="Update status for all bugs, legacy data correction",
        action='store_true',
    )
    mode.add_argument(
        "--generate-site", action="store_true", help="(Re)generate website"
    )

    return parser


def get_options(args=None):
    """
    get_options - use argparse to parse args, and return a
    argparse.Namespace, possibly with some changes / expansions /
    validatations.

    Client code should call this method with args as per sys.argv[1:],
    rather than calling make_parser() directly.

    Args:
        args ([str]): arguments to parse

    Returns:
        argparse.Namespace: options with modifications / validations
    """
    parser = make_parser()
    opt = parser.parse_args(args)
    opt._parser = parser

    # modifications / validations go here

    return opt


def load_data(opt):
    LEVELS[:] = [
        Level(**i)
        for i in json.load(
            open(os.path.join(os.path.dirname(__file__), 'levels.json'))
        )
    ]
    specials = os.path.join(opt.build, 'specials.json')
    if os.path.exists(specials):  # doesn't exist during boot-strap
        SPECIALS.update(json.load(open(specials)))
        # for list_ in SPECIALS.values():
        #     list_[:] = [i.replace('_', ' ') for i in list_]

    reader = csv.reader(open(os.path.join(opt.art, 'special.csv')))
    fields = [i.strip().replace(' ', '').lower() for i in next(reader)]
    for row in reader:
        bug = {k: v.strip() for k, v in zip(fields, row)}
        SPECIAL[
            bug['bugname'].replace(' Bug', '').replace(' ', '_')
        ] = Special(
            bugname=bug['bugname'],
            pts=int(bug['points']),
            comment=bug['taxonomy'],
            bio=bug['description'],
            color=bug['color'] or 'black',
        )


def score_for(lp_bug_id, fake=False):
    notes = []

    # 5 points - base line score for all bugs
    notes.append(
        ScoreNote(lp_bug_id=lp_bug_id, pts=5, comment="Basic bug score")
    )

    for special in SPECIALS.get(str(lp_bug_id), []):
        spec = SPECIAL[special]
        notes.append(
            SpecialScoreNote(
                special=special,
                bugname=spec.bugname,
                comment=spec.comment,
                pts=spec.pts,
                lp_bug_id=lp_bug_id,
            )
        )

    if fake:  # don't fetch the bug
        notes.append(
            ScoreNote(lp_bug_id=lp_bug_id, pts=15, comment="Fake test score")
        )
        return sum(i.pts for i in notes), notes

    bug = get_lp_bug(lp_bug_id)
    lp_bug = bug['bug']

    # 1 point - per comment on the bug
    msgs = int(lp_bug['message_count'] - 1)
    if msgs:
        notes.append(
            ScoreNote(
                lp_bug_id=lp_bug_id,
                pts=msgs,
                comment="%d comments on bug" % msgs,
            )
        )

    # 1 point - per hundred non-blank lines of comments on the bug
    sloc = 0
    authors = set()
    for msg in lp_bug['messages']:
        content = msg['content'].split('\n')
        content = [i for i in content if i.strip()]
        sloc += len(content)
        authors.add(msg['author']['username'])
    sloc = sloc // 100
    if sloc:
        notes.append(
            ScoreNote(
                lp_bug_id=lp_bug_id,
                pts=sloc,
                comment="%d hundred lines of discussion" % sloc,
            )
        )

    # 2 points - per person commenting on the bug, not counting the
    # original reporter
    authors = max(0, len(authors) - 1)
    if authors:
        notes.append(
            ScoreNote(
                lp_bug_id=lp_bug_id,
                pts=2 * authors,
                comment="%d people in discussion, 2pts each" % authors,
            )
        )

    # 3 points - per attachment on the bug
    if lp_bug['attachment_count']:
        notes.append(
            ScoreNote(
                lp_bug_id=lp_bug_id,
                pts=3 * lp_bug['attachment_count'],
                comment="%d attachments, 3pts each"
                % lp_bug['attachment_count'],
            )
        )

    if bug.get('status') in UNCLOSED:
        notes = [
            ScoreNote(
                lp_bug_id=lp_bug_id,
                pts=0,
                comment=(
                    "this bug (so far), status still '%s'" % bug['status']
                ),
            )
        ]

    return sum(i.pts for i in notes), notes


class BugGenerator:
    def __init__(self, opt):
        self.opt = opt
        self.log = []
        self.errors = False
        self.start_time = time.time()
        logtime = time.gmtime(self.start_time)
        self.asctime = time.strftime("%Y-%m-%d %H:%M", logtime)
        self.new_discoveries = {}
        self.total_bugs = 0

    def run(self):
        """Compile scores, generate output."""
        # generate author data and check for multiple claims
        authors, multiple = self.auth_mult()
        # process authors
        specials = self.proc_authors(authors, multiple)
        # get scores for authors
        authors = self.score_authors(specials)
        # generate outputs
        self.outputs(authors)
        self.write_new_discoveries()

    def auth_mult(self):
        """generate author data and check for multiple claims"""

        authors = defaultdict(lambda: {'gl': {}, 'lp': {}})

        # X gl_proj = get_val('gl_project')  # for project names for URLs

        # bug claimed by notes on GL:
        # Migrated from: https://bugs.launchpad.net/inkscape/+bug/xxx
        seen = set()
        for closure in scoring_gl_issues():
            authors[closure.gl_user]['gl'][closure.lp_bug_id] = closure
            gl_bug = get_gl_bug(closure.gl_bug_id)
            # X pprint(gl_bug['bug'])
            self.log.append(
                "INF: %s claims LP:%s in GL: %s"
                % (
                    closure.gl_user,
                    closure.lp_bug_id,
                    gl_bug['bug']['web_url'],
                )
            )
            seen.add(closure.lp_bug_id)

        # bugs claimed by Closed by: https://gitlab.com/username
        # notes in LP
        for closure in scoring_lp_issues():
            lp_bug_id = closure.lp_bug_id
            username = closure.gl_user
            bug = get_lp_bug(lp_bug_id)
            authors[username]['lp'][lp_bug_id] = BugClosure(
                gl_bug_id=None,
                lp_bug_id=lp_bug_id,
                gl_user=username,
                time=bug['bug']['date_last_updated'],
            )
            self.log.append("INF: %s claims LP:%s" % (username, lp_bug_id))
            if 'bug-migration' not in bug['bug']['tags']:
                self.log.append(
                    "ERR: %s didn't set bug-migration on LP:%s"
                    % (username, lp_bug_id)
                )
                if os.environ.get('BUGSTATS_MODIFY'):
                    lp_add_migration_tag(lp_bug_id)

            seen.add(lp_bug_id)

        if os.environ.get('BUGSTATS_FAKE_ALL'):
            gl_users = get_gl_userlist()
            extras = []
            for fake in range(2):
                extras.extend([i + ("_fake%d" % fake) for i in gl_users])
            gl_users += extras
            for lp_bug_id in get_val('all_lp_bug_ids'):
                if lp_bug_id in seen:
                    continue
                username = random.sample(gl_users, 1)[0]
                authors[username]['lp'][lp_bug_id] = BugClosure(
                    gl_bug_id=-999999,
                    lp_bug_id=lp_bug_id,
                    gl_user=username,
                    time=time.time(),
                )
                self.log.append(
                    "INF: %s claims LP:%s (FAKE)" % (username, lp_bug_id)
                )

        # check for multiple claims
        claimants = defaultdict(lambda: set())
        multiple = set()
        for author in authors:
            for closure in authors[author]['gl'].values():
                claimants[closure.lp_bug_id].add(author)
            for closure in authors[author]['lp'].values():
                claimants[closure.lp_bug_id].add(author)
        for lp_bug_id in claimants:
            multi = False
            if len(claimants[lp_bug_id]) == 2:
                closers = [
                    j
                    for i in authors.values()
                    for j in i['gl'].values()
                    if j.lp_bug_id == lp_bug_id
                ]
                closers.extend(
                    j
                    for i in authors.values()
                    for j in i['lp'].values()
                    if j.lp_bug_id == lp_bug_id
                )
                sources = set(i.gl_bug_id for i in closers)
                # When author A on LP closes the bug by pointing to a bug
                # by author B on GL, A should get credit for the closure.
                if None in sources and len(sources) == 2:
                    gl_close = [
                        i
                        for i in closers
                        if i.gl_bug_id is not None  # i.e the GL bug
                    ][0]
                    self.log.append(
                        "INF: Dropping claim for %s from %s"
                        % (gl_close.lp_bug_id, gl_close.gl_bug_id)
                    )
                    del authors[gl_close.gl_user]['gl'][gl_close.lp_bug_id]
                else:
                    multi = True
                    print(closers)
            elif len(claimants[lp_bug_id]) != 1:
                multi = True
            if multi:
                self.log.append(
                    "ERR: claims for %s: %s"
                    % (lp_bug_id, claimants[lp_bug_id])
                )
                multiple.add(lp_bug_id)
                self.errors = True

        return authors, multiple

    def proc_authors(self, authors, multiple, main_run=True):
        """process authors"""

        if main_run:
            # outerloop, recursive calls to get ranks
            self.proc_authors(authors, multiple, main_run=False)
            ranks = {i[0]: n + 1 for n, i in enumerate(self.scores)}
            non_zero = len([i for i in self.scores if i[1]])
        else:
            non_zero = 0  # count of non-zero scores

        # calc. individual self.scores
        self.scores = []
        specials = {}
        self.discovery_times = {
            k: Discovery(**v) for k, v in get_val('discovery_times').items()
        }  # previous sightings

        self.total_bugs = 0
        for author in authors:
            author_bugs = 0
            specials[author] = []
            auth_log = [
                {'title': "Run at %s Z" % self.asctime, 'class': 'datestamp'}
            ]
            total = 0
            # this silently ignores multiple claims for the same bug,
            # e.g. using both migration and closing annotations mistake
            # use dict for the set() like behavior of its keys while keeping
            # track of the source
            scoring = {
                closure.lp_bug_id: closure
                for closure in authors[author]['lp'].values()
            }
            scoring.update(
                {
                    closure.lp_bug_id: closure
                    for closure in authors[author]['gl'].values()
                }
            )
            for lp_bug_id in set(scoring) & multiple:
                auth_log.append(
                    {
                        'title': "No score for %s, multiple claimants"
                        % lp_bug_id,
                        'class': 'conflict',
                    }
                )
            # now drop the conflicting claims
            scoring = {k: v for k, v in scoring.items() if k not in multiple}

            for lp_bug_id, score in scoring.items():
                self.total_bugs += 1
                author_bugs += 1
                pts, notes = score_for(
                    int(lp_bug_id), fake=score.gl_bug_id == -999999
                )
                specials[author].extend(notes)
                total += pts
                if score.gl_bug_id:
                    gl_bug = get_gl_bug(score.gl_bug_id)
                    url = gl_bug['bug']['web_url']
                    if '/issues/' in url and '/-/issues/' not in url:
                        # GitLab changed their links, cached data may be old
                        url = url.replace('/issues/', '/-/issues/')
                    txt = '/'.join(url.split('/')[-4:])
                    gl = " from <a target='gl_%s' href='%s'>%s</a>" % (
                        txt,
                        url,
                        txt,
                    )
                else:
                    gl = ""
                lp = (
                    "<a target='lp_%s' "
                    "href='https://bugs.launchpad.net/inkscape/+bug/%s'>%s</a>"
                    % (lp_bug_id, lp_bug_id, lp_bug_id)
                )
                auth_log.append(
                    {
                        'title': "%s for %s%s" % (pts, lp, gl),
                        'class': 'bugscore',
                        'entries': [],
                    }
                )
                for note in notes:
                    if isinstance(note, SpecialScoreNote):
                        auth_log[-1]['special'] = note.special
                        auth_log[-1]['entries'].append(
                            {
                                'text': "%s - You found a %s Bug!"
                                % (note.pts, note.special),
                                'class': 'special',
                            }
                        )
                        mintime = None
                        if lp_bug_id in authors[author]['lp']:
                            mintime = authors[author]['lp'][lp_bug_id].time
                        if lp_bug_id in authors[author]['gl']:
                            gltime = authors[author]['gl'][lp_bug_id].time
                            if mintime:
                                mintime = min(mintime, gltime)
                            else:
                                mintime = gltime
                        if (
                            mintime
                            and mintime > 0
                            and (
                                note.special not in self.discovery_times
                                or self.discovery_times[note.special].time
                                > mintime
                            )
                        ):
                            self.discovery_times[note.special] = Discovery(
                                time=mintime, gl_user=author
                            )
                            self.new_discoveries[
                                note.special
                            ] = self.discovery_times[note.special]

                    if isinstance(note, ScoreNote):
                        auth_log[-1]['entries'].append(
                            {'text': "%s for %s" % (note.pts, note.comment)}
                        )
            self.scores.append((author, total))
            if main_run:
                auth_log.append(
                    {
                        'title': "%s total bug points!" % total,
                        'class': 'total',
                        'entries': [
                            {
                                'text': 'Current leaderboard rank: '
                                '%d out of %d<br/>%s bug%s'
                                % (
                                    ranks[author],
                                    non_zero,
                                    author_bugs,
                                    's' if author_bugs > 1 else '',
                                ),
                                'class': 'rank',
                            }
                        ],
                    }
                )
                auth_log.sort(key=lambda x: x['title'].startswith('0 for '))
                self.write_log(
                    auth_log,
                    author,
                    os.path.join(self.opt.build, "log/%s.html" % author),
                )

        put_val(
            'discovery_times',
            {k: v._asdict() for k, v in self.discovery_times.items()},
        )

        self.scores.sort(key=lambda x: x[1], reverse=True)

        return specials

    def write_log(self, auth_log, gl_username, filepath):
        """write individual author logs using template system

        Args:
            auth_log (list): title/class/entries dicts
            gl_username (str): GitLab username
            filepath (str): path to write log on
        """
        src_dir = os.path.join(os.path.dirname(__file__), 'templates')
        env = jinja2.Environment(loader=jinja2.FileSystemLoader(src_dir))
        template = env.get_template('log.html')
        context = dict(
            updated=self.asctime,
            blocks=auth_log,
            depth=1,
            gl_username=gl_username,
        )
        with open(filepath, 'wb') as out:
            out.write(template.render(context).encode('utf-8'))

    def score_authors(self, specials):
        """get scores for authors"""

        authors = []
        rank = 1
        ties = 0
        for author, score in self.scores:
            stats = {}
            if authors:
                if authors[-1]['score'] == score:
                    ties += 1
                else:
                    rank += 1 + ties
                    ties = 0
            stats['rank'] = rank
            authors.append(stats)
            lvl = [score >= i.score for i in LEVELS]
            lvl_i = lvl.index(True)
            lvl = LEVELS[lvl_i]
            stats['level'] = lvl.name
            stats['level_pts'] = lvl.score
            stats['score'] = score
            stats['username'] = author
            if lvl.name != LEVELS[0].name:
                stats['percent'] = int(
                    (score - LEVELS[lvl_i].score)
                    / (LEVELS[lvl_i - 1].score - lvl.score)
                    * 100
                )
                stats['next_lvl'] = LEVELS[lvl_i - 1].score - score
            stats['specials'] = []
            multi = {}
            for note in specials[author]:
                if isinstance(note, SpecialScoreNote):
                    if note.special in multi:
                        multi[note.special]['count'] += 1
                    else:
                        info = note._asdict()
                        multi[note.special] = info
                        info['count'] = 1
                        info['href'] = "./fieldguide.html#%s" % note.special
                        stats['specials'].append(info)
        return authors

    def outputs(self, authors):
        """generate outputs"""

        bug_count = defaultdict(lambda: 0)
        for author in authors:
            for spec in author['specials']:
                bug_count[spec['special']] += spec['count']

        src_dir = os.path.join(os.path.dirname(__file__), 'templates')
        env = jinja2.Environment(loader=jinja2.FileSystemLoader(src_dir))
        path = {
            'index': "index.html",
            'scoreboard': "leaderboard.html",
            'catalog': "fieldguide.html",
            'list': "catalog.lst",
            'contact': "contact.html",
            'log': "log/bugstats_log.html",
        }
        links = [
            {'url': path['index'], 'text': "Home"},
            {'url': path['scoreboard'], 'text': "Leaderboard"},
            {'url': path['catalog'], 'text': "Bug list"},
            {'url': path['contact'], 'text': "Contact"},
        ]
        base_context = dict(links=links, updated=self.asctime)
        path = {k: os.path.join(self.opt.build, v) for k, v in path.items()}

        for subdir in path.values():
            subdir = os.path.dirname(subdir)
            if not os.path.exists(subdir):
                os.makedirs(subdir)

        for static in 'index', 'contact':
            template = env.get_template('%s.html' % static)
            context = dict(
                base_context, page={'url': "%s.html" % static}, users=authors
            )
            with open(path[static], 'wb') as out:
                out.write(template.render(context).encode('utf-8'))

        template = env.get_template('scoreboard.html')
        context = dict(
            base_context,
            page={'url': "leaderboard.html"},
            users=[i for i in authors if i['score']],
            total_bugs=self.total_bugs,
            tot_perc=self.total_bugs / TOTAL_BUGS,
            all_bugs=TOTAL_BUGS,
        )
        with open(path["scoreboard"], 'wb') as out:
            out.write(template.render(context).encode('utf-8'))

        template = env.get_template('catalog.html')
        bugs = [
            dict(v._asdict(), name=k)
            for k, v in SPECIAL.items()
            if k in self.discovery_times
        ]
        for bug in bugs:
            discovery = self.discovery_times[bug['name']]
            bug['count'] = bug_count[bug['name']]
            bug['by'] = discovery.gl_user
            bug['time'] = time.gmtime(discovery.time)
            bug['time'] = time.strftime("%Y-%m-%d %H:%M", bug['time'])
            bug['text'] = bug['bio'].split('*')
        context = dict(
            base_context,
            page={'url': "fieldguide.html"},
            bugs=bugs,
            alpha=sorted(bugs, key=lambda x: x['name'].lower()),
        )
        with open(path["catalog"], 'wb') as out:
            out.write(template.render(context).encode('utf-8'))
        # list of known bugs for build_site.sh
        with open(path["list"], 'w') as out:
            for bug in context['bugs']:
                out.write("%s\n" % bug['name'])

        with open(path["log"], 'w') as logfile:
            logfile.write("<pre>\nINF: run at %s\n" % self.asctime)
            logfile.write('\n'.join(self.log))
            logfile.write("</pre>\n")

    def generate_specials(self, update=False):
        """Make specials.json, listing assignment of Bugs to bugs"""
        if update:
            all_lp_bugs = get_all_lp_bugs()
            put_val('all_lp_bug_ids', list(all_lp_bugs))
        else:
            all_lp_bugs = get_val('all_lp_bug_ids')
        all_lp_bugs = list(all_lp_bugs)
        print(len(all_lp_bugs))
        specials = os.listdir(os.path.join(self.opt.art, "img"))
        target = '_Bug_300.gif'
        specials = [i.replace(target, '') for i in specials if target in i]
        pool = []
        for special in specials:
            pool.extend([special] * 50)
        shuffle(all_lp_bugs)
        return {j: [i] for i, j in zip(pool, all_lp_bugs)}

    def update_gitlab(self):
        """Update GitLab data"""
        update_gl_issues(max_age=self.opt.update_gitlab)

    def update_launchpad(self):
        """Update Launchpad data"""
        update_lp_issues(max_age=self.opt.update_launchpad)

    def write_new_discoveries(self):
        """Write out any new discoveries

        Always creates a (possibly empty) file - i.e. clears old content"""
        with open(
            os.path.join(self.opt.build, 'new_discoveries.txt'), 'w'
        ) as out:
            for k, v in self.new_discoveries.items():
                out.write(
                    "%s by %s at %s\n"
                    % (
                        k,
                        v.gl_user,
                        time.strftime(
                            "%Y-%m-%d %H:%M:%S Z", time.gmtime(v.time)
                        ),
                    )
                )


def main():
    opt = get_options()
    keyvalstore.DB_FILENAME = opt.db
    load_data(opt)

    if opt.generate_specials:
        specials = BugGenerator(opt).generate_specials()
        if os.path.exists(opt.generate_specials):
            raise Exception("'%s' already exists" % opt.generate_specials)
        with open(opt.generate_specials, 'w') as out:
            json.dump(specials, out, indent=0, sort_keys=True)
    elif opt.update_gitlab:
        BugGenerator(opt).update_gitlab()
    elif opt.update_launchpad:
        BugGenerator(opt).update_launchpad()
    elif opt.generate_site:
        BugGenerator(opt).run()
    elif opt.fix_status:
        get_all_lp_status()
    else:
        opt._parser.print_help()


if __name__ == "__main__":
    main()
