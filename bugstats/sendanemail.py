# coding: utf-8
"""Carefully send an email to test requirements for servers
"""

import logging

logging.basicConfig(level=logging.DEBUG)

import smtplib
import configparser
import os
import sys
import argparse
import mimetypes
import itertools
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.mime.audio import MIMEAudio
from email.mime.application import MIMEApplication

_Type = {
    'image': MIMEImage,
    'audio': MIMEAudio,
    'text': MIMEText,
    'application': MIMEApplication,
}


def make_parser():
    class append_type(argparse.Action):
        def __call__(self, parser, namespace, values, option_string=None):
            if (
                not hasattr(namespace, self.dest)
                or getattr(namespace, self.dest) is None
            ):
                setattr(namespace, self.dest, [])
            getattr(namespace, self.dest).append(
                (option_string.strip('-'), values)
            )

    parser = argparse.ArgumentParser(
        description='Send email.', fromfile_prefix_chars='@'
    )
    for i in sorted(
        (
            'From',
            'password',
            'username',
            'server',
            'port',
            'Subject',
            'Reply-To',
            'configfile',
            'configname',
        )
    ):
        parser.add_argument('--' + i.lower())
    for i in sorted(('To', 'Cc', 'BCc')):
        for j in '', '-file':
            parser.add_argument(
                '--' + i.lower() + j, action='append', nargs='+', default=[]
            )
    parser.add_argument(
        '--dummy-list',
        action='store_true',
        help='generate 1000 nonsense email addresses for testing',
    )
    parser.add_argument(
        '--msg-file',
        action=append_type,
        help='Include text messages from files (as is)',
        dest='parts',
        metavar='MSGFILE',
        nargs='+',
    )
    parser.add_argument(
        '--attach',
        action=append_type,
        metavar='ATTACHMENT',
        help='Attach a file (base64 enc.)',
        dest='parts',
        nargs='+',
    )
    parser.add_argument(
        '--msg',
        action=append_type,
        help='Include message from command line argument',
        dest='parts',
        metavar='MSG',
        nargs='+',
    )
    parser.add_argument(
        '--stdin',
        action=append_type,
        nargs=0,
        dest='parts',
        help="Take a msg. from stdin even if there's a --msg‑file/--msg "
        "argument. Ordering is preserved, stdin is included in the msg. "
        "between any --msg* or --attach flags bracketing the --stdin flag",
    )
    parser.add_argument(
        '--recipients',
        type=int,
        metavar='N',
        help="Send in blocks of N recipients at a time",
    )
    parser.add_argument(
        '--alternate',
        action='store_true',
        help="Assume text and HTML versions and send as alternatives "
        "NOT IMPLEMENTED",
    )
    parser.add_argument(
        '--test',
        action='store_true',
        help="Just test the inputs and return the recipients",
    )
    parser.add_argument(
        '--ssl-connect',
        action='store_true',
        help="Assume SSL connection, don't try starttls",
    )
    return parser


def addresses(addrs):
    """return a unique list of addresses from addrs"""
    all = set()
    if isinstance(addrs, str):
        addrs = addrs.split(',')
    for thing in addrs:
        if isinstance(thing, list):
            i = thing
        else:
            i = thing.split(',')
        for j in i:
            all.update([j.lower()])

    return all


def sendanemail(**kargs):

    args = (
        'msg',
        'To',
        'From',
        'password',
        'username',
        'server',
        'port',
        'Subject',
        'Cc',
        'Bcc',
        'Reply-To',
        'ssl_connect',
    )

    # look for other keys we should pull from parser defaults / config.
    # (see below, arg keys are looked for in config. .ini file)
    parser = make_parser()
    args += tuple(vars(parser).keys())

    # also attachments, configfile, and configname and others

    arg = dict(kargs)

    if 'msg' not in kargs and (
        'attachments' not in kargs or not kargs['attachments']
    ):
        print('Nothing to do')
        return

    if 'configfile' not in arg or not arg['configfile']:
        arg['configfile'] = os.path.join(
            os.path.expanduser('~'), '.sendanemail.ini'
        )

    if 'configname' not in arg or not arg['configname']:
        arg['configname'] = 'default'

    config = None
    if os.path.isfile(arg['configfile']):
        config = configparser.ConfigParser()
        config.read(arg['configfile'])

    for i in args:
        if i not in arg or arg[i] is None:
            if config and config.has_option(arg['configname'], i):
                arg[i] = config.get(arg['configname'], i)

    # fix capitialization etc.
    lower = {i.lower().replace('-', '_'): i for i in args}
    fixed = {}
    for i in arg:
        if i in lower and arg[i] is not None:
            fixed[lower[i]] = arg[i]
            # del arg[i]
    arg.update(fixed)

    # we know we have msg or at least one attachment
    if (
        'From' not in arg
        or 'server' not in arg
        or arg['From'] is None
        or arg['server'] is None
    ):
        raise Exception('not enough params for email')

    msg = None

    if 'msg' in arg and arg['msg']:
        msg = MIMEText(arg['msg'])

    if 'attachments' in arg and arg['attachments']:
        _msg = MIMEMultipart()
        if not msg and not arg['attachments'][0][0].startswith('text'):
            _msg.preamble = 'Files attached'
        if msg:
            _msg.attach(msg)
        msg = _msg

        for i in arg['attachments']:
            majmin, payload = i  # attachments are (mimetype, payload)
            majmin = majmin.split('/')
            if len(majmin) > 1:
                maj, min_ = majmin
            else:
                maj, min_ = majmin, None

            if not isinstance(payload, str):
                payload = payload.read()

            if maj not in _Type:
                maj = 'application'

            if min_:
                part = _Type[maj](payload, min_)
            else:
                part = _Type[maj](payload)

            msg.attach(part)

    recips = {}
    for i in 'Bcc', 'To', 'Cc':
        emails = list(arg.get(i, []))
        for f in arg.get(i.lower() + '_file', []):
            emails += [line.strip() for line in open(f)]
        recips[i] = addresses(emails)

    recips['To'] -= recips['Bcc']
    recips['Cc'] -= recips['Bcc']
    recips['Cc'] -= recips['To']

    for i in 'Bcc', 'To', 'Cc':
        if recips[i]:
            arg[i] = ','.join(sorted(recips[i]))
        else:
            if i in arg:
                del arg[i]

    to = sorted(recips['To'] | recips['Cc'] | recips['Bcc'])

    for k in arg:
        if arg[k] and k[0].isupper():
            msg[k] = arg[k]

    testing = 'test' in arg and arg['test']

    if not testing:

        logging.debug("Server: '%s' Port: '%s'" % (arg['server'], arg['port']))

        starttls = True
        connector = smtplib.SMTP
        server_port = [arg['server']]

        if arg.get('port'):
            server_port += [arg['port']]

        if arg.get('ssl_connect'):
            starttls = False
            connector = smtplib.SMTP_SSL

        try:
            server = connector(*server_port)
        except smtplib.SMTPServerDisconnected:
            if connector != smtplib.SMTP_SSL:
                server = smtplib.SMTP_SSL(*server_port)
                starttls = False
            else:
                raise

        # server.set_debuglevel(1)
        if starttls:
            try:
                server.starttls()
            except smtplib.SMTPException:
                print("Note: starttls() failed")

        server.ehlo()
        server.login(arg['username'], arg['password'])

    if 'recipients' in arg and arg['recipients']:
        block_size = int(arg['recipients'])
        blocks = len(to) / block_size
        if blocks * block_size < len(to):
            blocks += 1
    else:
        block_size = 10000000
        blocks = 1

    for i in range(blocks):
        if not testing:
            server.sendmail(
                arg['From'],
                to[i * block_size : (i + 1) * block_size],
                msg.as_string(),
            )
            print(
                'Sent',
                to[i * block_size],
                '...',
                to[: (i + 1) * block_size][-1],
            )
        else:
            print(
                'Test',
                to[i * block_size],
                '...',
                to[: (i + 1) * block_size][-1],
            )

    if not testing:
        server.quit()


if __name__ == '__main__':
    arg = make_parser().parse_args()
    if arg.parts is None:
        arg.parts = []

    if arg.dummy_list:
        for i in range(1000):
            print("user%04d@example.com" % i)
        exit(0)

    parts = []

    for n, i in enumerate(arg.parts):
        if i[0] in ('stdin',):
            parts.append(('text/plain', getattr(sys, i[0])))
        elif i[0] == 'msg':
            for j in i[1]:
                parts.append(('text/plain', j))
        elif i[0] == 'msg-file':
            for j in i[1]:
                parts.append(
                    ('text/plain', open(j, 'rb').read().decode('utf-8'))
                )
        elif i[0] == 'attach':
            for j in i[1]:
                mtype, enc = mimetypes.guess_type(j)
                if not mtype:
                    mtype = "application/octet-stream"
                parts.append((mtype, open(j, 'rb')))

    arg.attachments = parts

    args = {}
    for k, v in vars(arg).items():
        if isinstance(v, list) and v and isinstance(v[0], list):
            # flatten lists of lists
            args[k] = list(itertools.chain(*v))
        # elif v is not None:
        else:
            args[k] = v
    # print(args)

    sendanemail(**args)
