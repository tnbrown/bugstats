from launchpad_sync import get_closer, get_moved_to


def test_get_closer():
    pfx = "Closed by: https://gitlab.com/"
    in_out = [
        ('ed', None),
        (pfx, None),
        (pfx + 'o', None),
        (pfx + 'ed', 'ed'),
        (pfx + 'smurf0', 'smurf0'),
        (pfx + '0smurf', None),
        ("Closed-by: https://gitlab.com/smurf0", 'smurf0'),
        ("Closed by https://gitlab.com/smurf0", 'smurf0'),
        ("Closed by: https/gitlab.com/smurf0", 'smurf0'),
        ("closedby HTTPS://gitlab.com/smurf0", 'smurf0'),
    ]
    for in_, out in in_out:
        assert get_closer(in_) == out


def OLD_test_get_moved_to():
    ptn = "[moved to GitLab](https://gitlab.com/inkscape/%s/issues/%s)"
    in_out = [
        ('ed', None),
        (ptn, None),
        (ptn % ('test', 123), ('test', '123')),
        (ptn % ('test', 'a123'), None),
        (ptn % ('', '123'), None),
    ]
    for in_, out in in_out:
        assert get_moved_to(in_) == out
