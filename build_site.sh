# NOTE: this runs in busybox as well as bash

set -e

echo
date
echo

SETTINGS="$1"
source "$SETTINGS"

# send a notification every SENDEVERY minutes, or on fail
SENDEVERY=240
# time of last notification
LASTNOTE=~/.last_bugstats_notification

if [ ! -e $LASTNOTE ]; then
    touch $LASTNOTE
fi

if [ "$DB" -a "$BUILD" -a "$SITE" -a "$ART" ]; then

    # this will abort (set -e) if any of these is absent
    ls -d "$SRC" "$BUILD" "$SITE" "$ART" >/dev/null

    if [ -e "$VENV" ]; then
        # doesn't have to exist, e.g. in Docker environment
        source "$VENV/bin/activate"
    fi

    mkdir -p "$BUILD"/log

    PROG="$SRC/bugstats/scoreboard.py"

    set +e

    while true; do

        echo -n Starting update:
        date
        RUNLOG=$(mktemp)
        date >$RUNLOG
        FAIL=

        if [ ! "$NO_UPDATE" ]; then
                python "$PROG" --db "$DB" --build "$BUILD" --art "$ART" \
                    --update-gitlab 300 && echo GL ok >> $RUNLOG || FAIL=1
                python "$PROG" --db "$DB" --build "$BUILD" --art "$ART" \
                    --update-launchpad 300 && echo LP ok >> $RUNLOG || FAIL=1
        fi
        python "$PROG" --db "$DB" --build "$BUILD" --art "$ART" \
            --generate-site && echo SITE ok >> $RUNLOG || FAIL=1

        cp -r "$BUILD"/*.html "$BUILD/log" "$SITE"
        cp -r "$SRC/bugstats/css" "$SITE"
        mkdir -p $SITE/img
        cp "$ART"/img/*.png "$SITE/img"
        cp "$ART"/img/*.jpg "$SITE/img"
        # copy over *only* the bugs already discovered
        for bug in $(cat "$BUILD/catalog.lst"); do
                cp $ART/img/"$bug"_Bug_50.gif $SITE/img
                cp $ART/img/"$bug"_Bug_300.gif $SITE/img
        done

        # set "FAIL" if LASTNOTE timestamp file *OLDER* than SENDEVERY minutes
        if [ "$(find $LASTNOTE -mmin +$SENDEVERY)" ]; then
            FAIL=1
        fi

        date >>$RUNLOG
        if [ "$RUN_EMAIL" -a "$FAIL" ]; then

            touch $LASTNOTE

            python "$SRC/bugstats/sendanemail.py" \
                --to "$RUN_EMAIL" --subject "INKSCAPE BUG MIGRATION RUNLOG" \
                --msg '(automatically generated message)' \
                --msg-file $RUNLOG
        fi
        if [ "$BUG_EMAIL" ]; then
            if [ "$(cat $BUILD/new_discoveries.txt)" ]; then
                python "$SRC/bugstats/sendanemail.py" \
                    --to "$BUG_EMAIL" --subject "INKSCAPE BUG MIGRATION DISCOVERY" \
                    --msg '(automatically generated message)' \
                    --msg-file $BUILD/new_discoveries.txt
            fi
        fi

        rm $RUNLOG

        SLEEP=900
        echo -n Update done:
        date
        echo -n Sleeping for $SLEEP seconds, next update:
        date -d "+$SLEEP seconds"
        sleep $SLEEP

    done

    if [ -e "$VENV" ]; then
        deactivate
    fi

else

    echo "MISSINING SETTINGS"

fi
