# Notes on deployment

## Names

`bug` - a bug or issue on Launchpad or GitLab

`Bug` - a collectable badge / sticker associated with a `bug`

## Installation / cron entry

```shell
mkdir bugstats
cd bugstats
git clone git@gitlab.com:tnbrown/bugstats.git
# note bugstats-art is private for now
git clone git@gitlab.com:tnbrown/bugstats-art.git
cd bugstats
virtualenv venv -p python3
./venv/bin/activate
pip install -r requirements.txt
cp settings.sh.template settings.sh

# first time setup (in virtualenv)
source settings.sh
# download list of bugs from LP and assign Bugs, creates "$BUILD/specials.json"
python bugstats/scoreboard.py --db "$DB" --build "$BUILD" --art "$ART" --gen-specials

```

Edit [settings.sh](./settings.sh.template) to configure directories, then

```cron
0 * * * * bash /path/to/build.sh /path/to/settings.sh
```

## Scanning Launchpad and GitLab

General theory is that we don't need to mirror the whole GitLab and LaunchPad 
bug catalogs, just the bugs changed in the last N days, starting whenever the
game starts.  By end of game we will have downloaded entire LP bug catalog.
Bug value is cached in the key / value store.  If the key / value store
(wrapper around SQLite / JSON db) is corrupted / lost, it can be reconstructed
from LP / GL data, with a larger "bugs changed since" scan.  First to find
data backup up separately, see below.

## Assigning Bugs to bugs

Could be done either randomly - when a new LP `bug` is claimed, randomly decide
if a bug is associated with it, and keep track of total `Bugs` issued, or
statically - download complete list of LP `bugs` and make one time assignment -
store in JSON file?

Random method would still require storing results, and would need rock solid
storage / backup, so go with static approach, simpler overall.
I think LP bug IDs can be listed without downloading the associated data.

Maybe do this in phases, so some bugs only become discoverable after a week,
two weeks, etc.  But still want to maintain the 1 in 5 ratio of bugs with Bugs?

## Discovery, first to find

~~Assuming game data is updated once an hour, give first find credit to
everyone who first finds a Bug within that hour.  First To Find (FTF) data
stored in key / value store, but also store in JSON files in `discoveries`
folder as backup, as hard to reconstruct if key / value store lost.~~

This is now based on bug message timestamps, so only one FTF per Bug.
In the extremely unlikely case of a discovery in sync. to the second
one will be chosen arbitrarily.

## Keeping undiscovered bugs hidden

Avoid having undiscovered bugs "browsable" by examining `.../imgs/` or
`.../buglist.json`.  While the images folder may not allow indexing,
and any bug-list JSON file may be unfindable, eliminate the possibility
altogether by copying only the found bugs into the public webspace.

Currently *this* repo. is public, although not publicized, the `bugstats-art`
repo. is private.


# Testing

There are some minor unit tests in `tests`, but more significantly three
environment variables:
```
BUGSTATS_FAKE_GL  - 20% chance of GL bug that doesn't close a LP bug being
                    randomly assigned a LP bug closure
BUGSTATS_FAKE_LP  - 20% chance of LP bug not listing closure being randomly
                    assigned a GL user for closure credit
BUGSTATS_FAKE_ALL - fake completion of all remaining LP bugs
```

Any combination of these environment variables can be set for testing.  They
may cause "multiple claim" errors.

To clear discovery times in testing - DO NO USE IN PRODUCTION:
```sql
sqlite3 -batch build/bugstat.db.sqlite "delete from keyval where key = 'discovery_times';" 
```

