# DRAFT DRAFT DRAFT DRAFT DRAFT DRAFT 

This is a **DRAFT** of a document for editing, and none of it content should not be taken literally at this point.

# The Great Inkscape Bug Migration

Help us migrate bugs by converting them from LaunchPad to GitLab's bug tracker. Win awards and swag by discovering special bugs and compete for top Bug Wrangler stats in our leaderboard.

NOTE: no coding is needed, you're not fixing bugs, you're just migrating them from LaunchPad to GitHub, or, for stale bugs, closing them on LaunchPad. Successfully closing a bug earns the same points as migrating.

## Instructions
 - If you don't have an account, sign up for one on Launchpad.
 - Pick an open bug [on LaunchPad](https://bugs.launchpad.net/).
 - A bug is open if:
    - (a) it's not tagged `bug-migration`
    - (b) its status is one of the following: 'New', 'Incomplete', 'Opinion', 'Confirmed', 'Triaged', 'In Progress'.
 - If the bug is open, you can tag it: 
    - Edit the bug's tags and add the `bug-migration` tag - doing this immediately minimizes the chances someone else will start working on the same bug.
    - Test whether the bug needs to be migrated to GitLab, or if it can be closed without further work on LaunchPad (because it's no longer valid etc.)

### Migration
If the Bug can be migrated:

  - Make a new bug report [on GitLab](https://gitlab.com/groups/inkscape/-/issues) and include, in the bug description, the following back link: `[LP:XXX](https://bugs.launchpad.net/inkscape/+bug/XXX)` where XXX is the LaunchPad bug number, e.g. 166554.  
  - Read over the comments associated with the bug in Launchpad, and write a summary comment in your new GitLab bug report.
  - In the closing comment on LaunchPad, include the following link: `[moved to GitLab](https://gitlab.com/inkscape/extensions/issues/42)` replacing the URL with the URL of the new issue on GitLab. 
  - Check attachments associated with the Launchpad bug make sense, and include them as needed in your GitLab bug report
  - It is not necessary to copy, verbatim, all the comments and attachments from the Launchpad bug to the GitLab bug, as we can still reference the Launchpad bug for details.  But please do copy / paste key comments and examples into your summary comment.

### Closing on LaunchPad

For stale bugs that can simply be closed on LaunchPad, with no new bug
on GitLab:

  - In the closing comment on LaunchPad, include the text: `Closed by: https://gitlab.com/username` replacing `username` with your GitLab username.  That way you get credit for closing the bug even though it isn't listed on GitLab.

## Rewards

Each LaunchPad bug converted to a GitLab bug or closed on LaunchPad will score points based on the bug's value, calculated [as shown below](#bug-scores). You can check your progress on [this leader board](FIXME).  Scores are calculated by a program which scans the bug lists about once an hour.

Collecting Special Bugs - Some bugs (issues on Launchpad) are special types which will automatically appear in your leaderboard specials once you have migrated (or closed) them. 
They are distributed throughout the entire bug population, so the more bugs you migrate the more opportunities you have to discover and collect them.
Once the event is over, we will ship you your awards stickers and a sticker for each special bug you have collected. Before this, you will have the opportunity to trade special bugs with each other (via email) to complete your collection, or get that special bug sticker you were hoping to find. Each time a new bug is discovered, an announcement will be made on our social media platforms who has found the first one, as well as an animated illustration of the bug.

Finding Special Bugs has multiple benefits. They add significant additional points to the [score for the associated “bug”](bug-scores) (issue).  You get to collect the set of badges on the [scoreboard](FIXME). 

There will also be a special prize for the top three highest scores.

## Bug scores

 - “bug”: one of about 5000 bugs reported on Launchpad
 - “Special Bug”: a collectable as described in [Rewards](rewards).

The goal of this game is to collectively migrate *all* of the open bugs from Launchpad to
GitLab.  

But some bugs will take more work to migrate than others, so points are assigned as follows:

 - 5 points - base line score for all bugs
 - 2 points - per person commenting on the bug, not counting the original reporter
 - 1 point - per comment on the bug
 - 1 point - per hundred non-blank lines of comments on the bug
 - 3 points - per attachment on the bug
 - XX points - for a Bug associated with the bug - these are the Bugs you can collect as described in [Rewards](#rewards).  You'll only find out you scored these extra points, and collected this Bug, when you've migrated the bug from Launchpad to GitLab.
